const accordeons = document.querySelectorAll('.accordeon');
const btns = document.querySelectorAll('.btn');

for (btn of btns) {
  btn.addEventListener('click', function (event) {
    if (event.target.parentNode.parentNode.classList.contains('active') == true) {
      event.target.parentNode.parentNode.classList.remove('active');
    } else {
      for (accordeon of accordeons) {
        accordeon.classList.remove('active');
      }
      event.target.parentNode.parentNode.classList.add('active');
    }
  })
}





// for (body of bodys) {
//   body.classList.remove('active');
// }
// sections[0].onclick = function () {
//   bodys[0].classList.toggle('active');
//   bodys[1].classList.remove('active');
//   bodys[2].classList.remove('active');
// }
// sections[1].onclick = function () {
//   bodys[1].classList.toggle('active');
//   bodys[0].classList.remove('active');
//   bodys[2].classList.remove('active');
// }
// sections[2].onclick = function () {
//   bodys[2].classList.toggle('active');
//   bodys[0].classList.remove('active');
//   bodys[1].classList.remove('active');
// }